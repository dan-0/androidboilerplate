package me.danlowe.boilerplate.ui

import kotlinx.coroutines.experimental.test.TestCoroutineContext
import me.danlowe.boilerplate.model.providers.coroutine.CoroutineContextProvider
import kotlin.coroutines.experimental.CoroutineContext

class TestCoroutineContextProvider: CoroutineContextProvider {
    val testContext = TestCoroutineContext()
    override val io: CoroutineContext = testContext
    override val ui: CoroutineContext = testContext
}