package me.danlowe.boilerplate.ui.quotes

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import me.danlowe.boilerplate.R
import me.danlowe.boilerplate.ui.main.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class QuotesUiTest {
    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(
            MainActivity::class.java)

    private lateinit var quoteTestResources: QuoteTestResources

    @Before
    fun setup() {
        quoteTestResources = QuoteTestResources("HELLO!")
        quoteTestResources.setup(activityRule)
    }

    @After
    fun cleanup() {
        quoteTestResources.cleanup()
    }

    @Test
    fun testDesiredQuoteDisplayed() {
        Espresso.onView(withId(R.id.navigation_quotes)).perform(click())
        Espresso.onView(withId(R.id.quotesLayout)).check(matches(isDisplayed()))
        Espresso.onView(withId(R.id.quotesText)).check(matches(withText("\"${quoteTestResources.response}\"")))
    }
}