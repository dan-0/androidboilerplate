package me.danlowe.boilerplate.ui.base

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class InfiniteScrollListener(private val layoutManager: LinearLayoutManager)
    : RecyclerView.OnScrollListener() {

    private var visibleThreshold: Int = 5


    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
        val totalItems = layoutManager.itemCount

        if (lastVisiblePosition + visibleThreshold > totalItems) {
            loadMore()
        }
    }

    abstract fun loadMore()
}