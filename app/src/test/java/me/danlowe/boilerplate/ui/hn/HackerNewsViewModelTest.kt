package me.danlowe.boilerplate.ui.hn

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.runBlocking
import me.danlowe.boilerplate.model.data.HackerNewsItem
import me.danlowe.boilerplate.model.providers.network.NetworkInfoProvider
import me.danlowe.boilerplate.model.service.retrofit.HackerNewsService
import me.danlowe.boilerplate.ui.TestCoroutineContextProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HackerNewsViewModelTest {

    private lateinit var networkInfoProvider: NetworkInfoProvider
    private lateinit var contextProvider: TestCoroutineContextProvider
    private lateinit var hackerNewsService: HackerNewsService
    private lateinit var ut: HackerNewsViewModel

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        networkInfoProvider = mock()
        contextProvider = TestCoroutineContextProvider()
        hackerNewsService = mock()
        ut = HackerNewsViewModel(networkInfoProvider, contextProvider, hackerNewsService)
    }

    @Test
    fun `happy path`() {

        whenever(networkInfoProvider.isNetworkAvailable()).thenReturn(true)

        // Test an edge case of only 7 results being returned
        val storyIds = arrayOf(
                1L,
                2L,
                2346525462345L,
                3,
                5,
                34,
                6
        )

        val mockDeferredStories: Deferred<Array<Long>> = mock()
        runBlocking {
            whenever(mockDeferredStories.await()).thenReturn(storyIds). then {
                throw HackerNewsTestException("Should only call once")
            }
        }
        whenever(hackerNewsService.getTopStories()).thenReturn(mockDeferredStories)

        val hnItems = storyIds.map {
            HackerNewsItem(id = it)
        }
        var hnItemCounter = hnItems.size

        val mockDeferredHnItem: Deferred<HackerNewsItem> = mock()

        runBlocking {
            whenever(mockDeferredHnItem.await()).thenReturn(hnItems[--hnItemCounter])
        }

        whenever(hackerNewsService.getItem(any())).thenReturn(mockDeferredHnItem)

        ut.viewInitialize()
        contextProvider.testContext.triggerActions()

        assertEquals("Total number of stories should match number of IDs", storyIds.size, ut.stories.value!!.size)
    }

    @Test
    fun `initialize with no network`() {
        whenever(networkInfoProvider.isNetworkAvailable()).thenReturn(false)

        // value is always set, so set to null so we can check against
        ut.stories.value = null

        ut.viewInitialize()
        contextProvider.testContext.triggerActions()

        assertEquals(0, ut.stories.value!!.size)
    }

    @Test
    fun `add to current stories`() {
        val originalStories = 15
        val storiesToLoad = 2
        val expectedStories = 10 + storiesToLoad

        whenever(networkInfoProvider.isNetworkAvailable()).thenReturn(true)


        // Test an edge case of only 7 results being returned
        val storyIds = (0L until originalStories).map { it }.toTypedArray()

        val mockDeferredStories: Deferred<Array<Long>> = mock()
        runBlocking {
            whenever(mockDeferredStories.await()).thenReturn(storyIds). then {
                throw HackerNewsTestException("Should only call once")
            }
        }
        whenever(hackerNewsService.getTopStories()).thenReturn(mockDeferredStories)

        val hnItems = mutableListOf<HackerNewsItem>()
        hnItems.addAll(storyIds.map {
            HackerNewsItem(id = it)
        })

        val mockDeferredHnItem: Deferred<HackerNewsItem> = mock()

        runBlocking {
            var itemStub = whenever(mockDeferredHnItem.await())
            (0 until expectedStories).forEach {
                itemStub = itemStub.thenReturn(hnItems.removeAt(0))
            }
            itemStub = itemStub.then { throw HackerNewsTestException("Called too many times") }
        }

        whenever(hackerNewsService.getItem(any())).thenReturn(mockDeferredHnItem)

        runBlocking {
            ut.viewInitialize()
            ut.loadStories(storiesToLoad)
            contextProvider.testContext.triggerActions()
        }


        assertEquals("Should equal total loaded stories", expectedStories, ut.stories.value!!.size)
        assertEquals("Remaining items should equal difference",
                hnItems.size,
                originalStories - ut.stories.value!!.size)
    }

    inner class HackerNewsTestException(msg: String): Exception(msg)
}