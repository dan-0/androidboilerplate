package me.danlowe.boilerplate.model.providers.network

interface NetworkInfoProvider {
    fun isNetworkAvailable(): Boolean
}