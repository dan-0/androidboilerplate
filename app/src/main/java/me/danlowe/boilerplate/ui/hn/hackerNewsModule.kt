package me.danlowe.boilerplate.ui.hn

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val hackerNewsModule = module {
    viewModel { HackerNewsViewModel(get(), get(), get()) }
    single { HackerNewsViewAdapter(arrayListOf()) }
}