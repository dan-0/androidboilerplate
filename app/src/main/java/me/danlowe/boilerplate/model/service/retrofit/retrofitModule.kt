package me.danlowe.boilerplate.model.service.retrofit

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import me.danlowe.boilerplate.BuildConfig
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val retrofitModule = module {

    single {
        Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
    }

    single {
        Retrofit.Builder()
                .baseUrl(BuildConfig.HN_URL)
                .addConverterFactory(MoshiConverterFactory.create(get()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
                .build()
                .create(HackerNewsService::class.java)
    }

    single {
        Retrofit.Builder()
                .baseUrl(BuildConfig.QUOTES_URL)
                .addConverterFactory(MoshiConverterFactory.create(get()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
                .build()
                .create(SwansonQuoteService::class.java)
    }

}