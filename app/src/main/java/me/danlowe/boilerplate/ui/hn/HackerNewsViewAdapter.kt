package me.danlowe.boilerplate.ui.hn

import android.content.Intent
import android.net.Uri
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.hacker_news_tile.view.*
import me.danlowe.boilerplate.R
import me.danlowe.boilerplate.model.data.HackerNewsItem
import me.danlowe.boilerplate.ui.base.BaseRecyclerViewAdapter
import me.danlowe.boilerplate.ui.base.BaseViewHolder
import timber.log.Timber

class HackerNewsViewAdapter(
        val stories: MutableList<HackerNewsItem>)
    : BaseRecyclerViewAdapter<HackerNewsItem, ConstraintLayout>(stories, R.layout.hacker_news_tile) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return ViewHolder(inflateLayout(parent))
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    inner class ViewHolder(view: ConstraintLayout): BaseViewHolder(view) {
        private val storyOrderView = view.hnStoryOrder
        private val titleView = view.hnTitleText
        private val tile = view.hnTile

        override fun onBind(position: Int) {
            Timber.d("Binding: $position")
            storyOrderView.text = "${(position + 1)}"

            val item = stories[position]

            titleView.text = item.title
            tile.setOnClickListener {
                val context = it.context
                if (item.url.isEmpty()) {
                    Snackbar.make(it, context.getString(R.string.cannot_access), Snackbar.LENGTH_LONG).show()
                } else {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(item.url))
                    it.context.startActivity(intent)
                }
            }
        }
    }
}