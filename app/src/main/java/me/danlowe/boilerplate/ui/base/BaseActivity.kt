package me.danlowe.boilerplate.ui.base

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import timber.log.Timber

abstract class BaseActivity<T : ViewDataBinding, out V : BaseViewModel<*>> : AppCompatActivity() {

    private var progressBar: ProgressBar? = null
    var viewDataBinding: T? = null
    abstract val actViewModel: V
    abstract val bindingVariable: Int

    @get:LayoutRes
    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        Timber.d("Base activity onCreate called")
        lifecycle.addObserver(actViewModel)
        super.onCreate(savedInstanceState)
        initDataBinding()
    }

    private fun initDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutId)
        viewDataBinding?.setVariable(bindingVariable, actViewModel)
        viewDataBinding?.executePendingBindings()
    }

    fun showLoading() {
        if (progressBar == null) {
            progressBar = getProgressBar() ?: return
        }
        Timber.d("Showing progress bar.")
        progressBar?.visibility = View.VISIBLE
    }

    fun hideLoading() {
        progressBar?.visibility = View.GONE
        Timber.d("Hiding progress bar.")
    }

    open fun getProgressBar(): ProgressBar? {
        return null
    }
}