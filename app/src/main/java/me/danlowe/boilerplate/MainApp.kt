package me.danlowe.boilerplate

import android.app.Application
import com.squareup.leakcanary.LeakCanary
import me.danlowe.boilerplate.model.providers.coroutine.coroutineContextModule
import me.danlowe.boilerplate.model.providers.network.networkModule
import me.danlowe.boilerplate.model.service.retrofit.retrofitModule
import me.danlowe.boilerplate.ui.hn.hackerNewsModule
import me.danlowe.boilerplate.ui.images.imagesModule
import me.danlowe.boilerplate.ui.main.mainActivityModule
import me.danlowe.boilerplate.ui.quotes.quoteModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class MainApp : Application() {

    private val koinModules = listOf(
            imagesModule,
            hackerNewsModule,
            quoteModule,
            mainActivityModule,
            retrofitModule,
            coroutineContextModule,
            networkModule
    )

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        LeakCanary.install(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            // TODO Add Crashlytics or other reporting tree
        }

        startKoin(this, koinModules)
    }
}