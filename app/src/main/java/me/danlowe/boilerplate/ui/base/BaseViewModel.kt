package me.danlowe.boilerplate.ui.base

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.experimental.CancellationException
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import me.danlowe.boilerplate.model.providers.coroutine.CoroutineContextProvider
import timber.log.Timber
import kotlin.coroutines.experimental.CoroutineContext

abstract class BaseViewModel<T>(private val coroutineContextProvider: CoroutineContextProvider)
    : ViewModel(), LifecycleObserver {

    var navigator: T? = null
    val isLoading = ObservableBoolean(false)

    private var initialized = false

    private var job: Job = Job()

    private fun groomJob(context: CoroutineContext, block: suspend CoroutineScope.() -> Unit) {
        val currentJob = CoroutineScope(context + job).launch { block.invoke(this) }

        currentJob.invokeOnCompletion {

            when(it) {
                null -> {}
                is CancellationException -> {}
                else -> Timber.e(it, "Exception occurred during execution")
            }
        }
    }

    protected fun launchIo(block: suspend CoroutineScope.() -> Unit) {
        groomJob(coroutineContextProvider.io, block)
    }

    protected fun launchMain(block: suspend CoroutineScope.() -> Unit) {
        groomJob(coroutineContextProvider.ui, block)
    }

    /**
     * A one time initialization function to help with testing. `init{}` isn't as controllable
     * in unit tests. This requires a view to exist in order to initialize the data
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun viewInitialize() {
        // job value is retained after onDestroy(), but in a canceled state so it needs to be reset
        if (job.isCancelled) {
            job = Job()
        }

        if (!initialized) {
            initialized = true
            initialize()
        }
    }

    protected open fun initialize() {}

    override fun onCleared() {
        Timber.d("Clearing jobs")
        job.cancel()

        super.onCleared()
    }
}