package me.danlowe.boilerplate.ui.images

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_images.*
import me.danlowe.boilerplate.R
import me.danlowe.boilerplate.databinding.FragmentImagesBinding
import me.danlowe.boilerplate.ui.base.BaseFragment
import me.danlowe.boilerplate.ui.base.InfiniteScrollListener
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * A placeholder fragment containing a simple view.
 */
class ImagesFragment : BaseFragment<FragmentImagesBinding, ImagesViewModel>(),
        ImagesNavigator {
    override val fragViewModel: ImagesViewModel by viewModel()
    override val layoutId: Int = R.layout.fragment_images

    private val contentAdapter: ImagesAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragViewModel.navigator = this
    }

    private fun setItemObserver() {
        val itemChangeObserver = Observer<ArrayList<Int>> @Synchronized {
            Timber.d("Images items changed: $it")

            var curItemsSize = contentAdapter.items.size

            if (curItemsSize > it.size) {
                contentAdapter.items.clear()
                curItemsSize = 0
            }

            if (it.size > curItemsSize) {
                contentAdapter.items.addAll(it.subList(curItemsSize, it.size))
                contentAdapter.notifyItemInserted(curItemsSize)
            }
        }
        fragViewModel.observableItems.observe(this, itemChangeObserver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setItemObserver()
        initView()
    }

    private fun initView() {

        imagesRecyclerView.apply {

            itemAnimator = DefaultItemAnimator()
            adapter = contentAdapter

            val linearLayoutManager = LinearLayoutManager(context)

            layoutManager = linearLayoutManager
            val scrollListener = object: InfiniteScrollListener(linearLayoutManager) {
                override fun loadMore() {
                    fragViewModel.addItems(10)
                }
            }

            addOnScrollListener(scrollListener)

            val divider = DividerItemDecoration(context, linearLayoutManager.orientation)
            val divDrawable = context.getDrawable(R.drawable.picture_divider)
            divider.setDrawable(divDrawable!!)
            addItemDecoration(divider)
        }
    }
}
