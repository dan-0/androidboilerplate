package me.danlowe.boilerplate.ui.quotes

import android.os.Bundle
import me.danlowe.boilerplate.R
import me.danlowe.boilerplate.databinding.FragmentQuotesBinding
import me.danlowe.boilerplate.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class QuotesFragment : BaseFragment<FragmentQuotesBinding, QuotesViewModel>(), QuotesNavigator {
    override val fragViewModel: QuotesViewModel by viewModel()
    override val layoutId: Int = R.layout.fragment_quotes

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragViewModel.navigator = this
        retainInstance = true
    }
}