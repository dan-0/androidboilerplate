package me.danlowe.boilerplate.ui.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<in T, V: View>(
        private val collection: Collection<T>,
        @LayoutRes private val layoutId: Int)
    : RecyclerView.Adapter<BaseViewHolder>() {


    protected fun inflateLayout(parent: ViewGroup): V {
        @Suppress("UNCHECKED_CAST")
        return LayoutInflater.from(parent.context).inflate(layoutId, parent, false) as V
    }

    override fun getItemCount(): Int {
        return collection.size
    }
}