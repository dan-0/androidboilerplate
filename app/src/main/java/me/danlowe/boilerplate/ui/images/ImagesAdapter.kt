package me.danlowe.boilerplate.ui.images

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import me.danlowe.boilerplate.R
import me.danlowe.boilerplate.model.glide.GlideApp
import me.danlowe.boilerplate.ui.base.BaseViewHolder
import timber.log.Timber

class ImagesAdapter(val items: MutableList<Int>) : RecyclerView.Adapter<BaseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.image_holder, parent, false) as ImageView
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    inner class ViewHolder(private val view: ImageView) : BaseViewHolder(view) {

        override fun onBind(position: Int) {
            Timber.d("Binding new images at position: $position")
            GlideApp.with(view.context)
                    .load("https://picsum.photos/500/200/?image=${items[position]}")
                    .placeholder(R.drawable.ic_image_black_500x200dp)
                    .into(view)
        }
    }
}