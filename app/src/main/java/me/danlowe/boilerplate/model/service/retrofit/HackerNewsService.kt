package me.danlowe.boilerplate.model.service.retrofit

import kotlinx.coroutines.experimental.Deferred
import me.danlowe.boilerplate.model.data.HackerNewsItem
import retrofit2.http.GET
import retrofit2.http.Path

interface HackerNewsService {

    @GET("v0/topstories.json")
    fun getTopStories(): Deferred<Array<Long>>

    @GET("v0/item/{id}.json")
    fun getItem(@Path("id") id: Long): Deferred<HackerNewsItem?>
}