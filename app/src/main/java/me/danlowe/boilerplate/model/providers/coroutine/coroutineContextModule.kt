package me.danlowe.boilerplate.model.providers.coroutine

import org.koin.dsl.module.module

val coroutineContextModule = module {
    single { MainCoroutineContextProvider() as CoroutineContextProvider }
}