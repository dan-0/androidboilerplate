package me.danlowe.boilerplate.ui.main

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val mainActivityModule = module {
    viewModel { MainViewModel(get()) }
}