package me.danlowe.boilerplate.koin.retrofit

import kotlinx.coroutines.experimental.Deferred
import me.danlowe.boilerplate.model.service.retrofit.SwansonQuoteService

class QuoteService(private val quoteValue: Deferred<Array<String>>) : SwansonQuoteService {
    override fun getQuote(): Deferred<Array<String>> {
        return quoteValue
    }
}