package me.danlowe.boilerplate.model.providers.coroutine

import kotlinx.coroutines.experimental.Dispatchers

class MainCoroutineContextProvider : CoroutineContextProvider {
    override val io = Dispatchers.IO
    override val ui = Dispatchers.Main
}