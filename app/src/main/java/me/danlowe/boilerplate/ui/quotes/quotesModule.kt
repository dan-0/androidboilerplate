package me.danlowe.boilerplate.ui.quotes

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val quoteModule = module {
    viewModel { QuotesViewModel(get(), get(), get()) }
}