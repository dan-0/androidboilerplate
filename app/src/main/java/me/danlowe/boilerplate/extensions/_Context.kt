package me.danlowe.boilerplate.extensions

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.HapticFeedbackConstants

internal fun Context.vibrateTap() {
    val vibe = this.getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator

    vibe ?: return
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        vibe.vibrate(VibrationEffect.createOneShot(HapticFeedbackConstants.KEYBOARD_TAP.toLong(), 1))
    } else {
        // Deprecated, but the replacement is only for 26+, so must use deprecated version
        @Suppress("DEPRECATION")
        vibe.vibrate(HapticFeedbackConstants.KEYBOARD_TAP.toLong())
    }
}