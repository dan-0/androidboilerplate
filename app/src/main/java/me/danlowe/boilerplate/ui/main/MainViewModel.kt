package me.danlowe.boilerplate.ui.main

import me.danlowe.boilerplate.model.providers.coroutine.CoroutineContextProvider
import me.danlowe.boilerplate.ui.base.BaseViewModel

class MainViewModel(contextProvider: CoroutineContextProvider) : BaseViewModel<MainNavigator>(contextProvider)