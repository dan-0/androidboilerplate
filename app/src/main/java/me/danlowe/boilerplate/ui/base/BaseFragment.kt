package me.danlowe.boilerplate.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.progress_bar_layout.*
import me.danlowe.boilerplate.BR
import timber.log.Timber

abstract class BaseFragment<T : ViewDataBinding, out V : BaseViewModel<*>> : Fragment() {

    private var baseActivity: BaseActivity<T, V>? = null
    private var viewDataBinding: T? = null
    private var rootView: View? = null

    private val bindingVariable = BR.viewModel

    val fragTag: String = javaClass.name

    abstract val fragViewModel: V

    @get:LayoutRes
    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        lifecycle.addObserver(fragViewModel)
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        rootView = viewDataBinding!!.root
        return rootView
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding!!.setVariable(bindingVariable, fragViewModel)
        viewDataBinding!!.executePendingBindings()
    }

    @Suppress("UNCHECKED_CAST")
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            val activity = context as? BaseActivity<T, V>
            this.baseActivity = activity
        }
    }

    override fun onDetach() {
        baseActivity = null
        Timber.d("Detaching fragment")
        super.onDetach()
    }

    open fun showLoading(msg: String) {
        Timber.d("Showing loading view.")
        loadingTextView?.text = msg
        loadingView?.visibility = View.VISIBLE
    }

    open fun hideLoading() {
        Timber.d("Hiding loading view.")
        loadingTextView?.text = ""
        loadingView?.visibility = View.GONE
    }
}