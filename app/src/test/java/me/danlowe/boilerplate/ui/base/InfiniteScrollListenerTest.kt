package me.danlowe.boilerplate.ui.base

import androidx.recyclerview.widget.LinearLayoutManager
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class InfiniteScrollListenerTest {

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var ut: InfiniteScrollListener
    private var loadMoreCalled = false

    @Before
    fun setup() {
        layoutManager = mock()
        ut = object : InfiniteScrollListener(layoutManager) {
            override fun loadMore() {
                loadMoreCalled = true
            }

        }
        loadMoreCalled = false
    }

    @Test
    fun `on Scrolled should load more`() {
        whenever(layoutManager.findLastVisibleItemPosition()).thenReturn(1)
        whenever(layoutManager.itemCount).thenReturn(5)

        ut.onScrolled(mock(), 1, 1)

        assertTrue(loadMoreCalled)
    }

    @Test
    fun `on Scrolled should NOT load more`() {
        whenever(layoutManager.findLastVisibleItemPosition()).thenReturn(1)
        whenever(layoutManager.itemCount).thenReturn(6)

        ut.onScrolled(mock(), 1, 1)

        assertFalse(loadMoreCalled)
    }
}