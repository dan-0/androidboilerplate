package me.danlowe.boilerplate.ui.images

import androidx.lifecycle.MutableLiveData
import me.danlowe.boilerplate.model.providers.coroutine.CoroutineContextProvider
import me.danlowe.boilerplate.ui.base.BaseViewModel
import timber.log.Timber
import java.util.*

class ImagesViewModel(
        contextProvider: CoroutineContextProvider)
    : BaseViewModel<ImagesNavigator>(contextProvider) {
    val observableItems = MutableLiveData<ArrayList<Int>>().apply { value = arrayListOf() }

    override fun initialize() {
        super.initialize()
        addItems(5)
    }

    @Synchronized
    fun addItems(numToAdd: Int) {
        Timber.d("Adding more images")
        val items = observableItems.value!!

        var addAmount = numToAdd
        if(items.size + numToAdd > 1000) {
            addAmount = 1000 - items.size
        }

        (0 until addAmount).forEach {
            items.add(Random().nextInt(999) + 1)
        }

        launchMain {
            observableItems.value = items
        }
    }
}