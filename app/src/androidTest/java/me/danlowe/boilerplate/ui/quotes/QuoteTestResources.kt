package me.danlowe.boilerplate.ui.quotes

import androidx.test.rule.ActivityTestRule
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import me.danlowe.boilerplate.TestResources
import me.danlowe.boilerplate.model.service.retrofit.SwansonQuoteService
import me.danlowe.boilerplate.ui.main.MainActivity
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.loadKoinModules

class QuoteTestResources(
        val response: String = "TEST"
) : TestResources() {

    val quoteServiceModule = module {
        single(override = true) {
            object: SwansonQuoteService {
                override fun getQuote(): Deferred<Array<String>> {
                    return scope().async {
                        arrayOf(response)
                    }
                }
            } as SwansonQuoteService }
    }

    override fun setup(testRule: ActivityTestRule<MainActivity>) {
        testRule.runOnUiThread { loadKoinModules(listOf(quoteServiceModule)) }
        super.setup(testRule)
    }
}