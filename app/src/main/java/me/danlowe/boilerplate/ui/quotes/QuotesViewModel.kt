package me.danlowe.boilerplate.ui.quotes

import androidx.databinding.ObservableField
import kotlinx.coroutines.experimental.TimeoutCancellationException
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.withTimeout
import me.danlowe.boilerplate.model.providers.coroutine.CoroutineContextProvider
import me.danlowe.boilerplate.model.providers.network.NetworkInfoProvider
import me.danlowe.boilerplate.model.service.retrofit.SwansonQuoteService
import me.danlowe.boilerplate.ui.base.BaseViewModel
import timber.log.Timber

class QuotesViewModel(
        coroutineContextProvider: CoroutineContextProvider,
        private val networkInfoProvider: NetworkInfoProvider,
        private val quoteService: SwansonQuoteService)
    : BaseViewModel<QuotesNavigator>(coroutineContextProvider) {

    val quote: ObservableField<String> = ObservableField("")
    val quoteDelay = 10000L

    override fun initialize() {
        super.initialize()
        launchMain {
            while (true) {
                try {
                    with(doWithTimeout()) {
                        if(!isEmpty()) {
                            quote.set("\"$this\"")
                        }
                    }
                } catch (e: TimeoutCancellationException) {
                    Timber.w(e, "Experienced timeout exception")
                }
                delay(quoteDelay)
            }
        }
    }

    /**
     * Retrieves and sets a new quote
     * @return Random quote
     */
    @Throws(TimeoutCancellationException::class)
    private suspend fun doWithTimeout(): String {
        return withTimeout(20000, {
            if (!networkInfoProvider.isNetworkAvailable()) {
                return@withTimeout ""
            }

            val quotes = quoteService.getQuote().await()
            if (quotes.isEmpty()) {
                Timber.w("Empty quotes array received")
                return@withTimeout ""
            }

            quotes[0]
        })
    }
}