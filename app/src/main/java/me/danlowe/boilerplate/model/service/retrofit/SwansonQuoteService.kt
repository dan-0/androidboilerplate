package me.danlowe.boilerplate.model.service.retrofit

import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.GET

interface SwansonQuoteService {
    @GET("v2/quotes")
    fun getQuote(): Deferred<Array<String>>
}