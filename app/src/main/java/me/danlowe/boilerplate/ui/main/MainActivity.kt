package me.danlowe.boilerplate.ui.main

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import me.danlowe.boilerplate.BR
import me.danlowe.boilerplate.R
import me.danlowe.boilerplate.databinding.ActivityMainBinding
import me.danlowe.boilerplate.extensions.vibrateTap
import me.danlowe.boilerplate.ui.base.BaseActivity
import me.danlowe.boilerplate.ui.base.BaseFragment
import me.danlowe.boilerplate.ui.hn.HackerNewsFragment
import me.danlowe.boilerplate.ui.images.ImagesFragment
import me.danlowe.boilerplate.ui.quotes.QuotesFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), MainNavigator {
    override val actViewModel: MainViewModel by viewModel()
    override val bindingVariable: Int = BR.viewModel
    override val layoutId: Int = R.layout.activity_main

    private var hnFragment = HackerNewsFragment()
    private var quotesFragment = QuotesFragment()
    private var imagesFragment = ImagesFragment()

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        Timber.d("Item selected: ${it.itemId}")
        when (it.itemId) {
            R.id.navigation_hn_stories -> {
                vibrateTap()
                loadMenuFragment(hnFragment)
                true
            }
            R.id.navigation_quotes -> {
                vibrateTap()
                loadMenuFragment(quotesFragment)
                true
            }
            R.id.navigation_images -> {
                vibrateTap()
                loadMenuFragment(imagesFragment)
                true
            }
            else -> false
        }
    }

    private fun loadMenuFragment(frag: BaseFragment<*,*>) {
        Timber.d("Adding fragment")
        supportFragmentManager.beginTransaction()
                .disallowAddToBackStack()
                .replace(R.id.view_content, frag, frag.fragTag)
                .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actViewModel.navigator = this
        navigation.setOnNavigationItemSelectedListener {
            onNavigationItemSelectedListener.onNavigationItemSelected(it)
        }

        navigation.selectedItemId = R.id.navigation_hn_stories
    }
}
