package me.danlowe.boilerplate.ui.quotes

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.TimeoutCancellationException
import kotlinx.coroutines.experimental.cancel
import kotlinx.coroutines.experimental.runBlocking
import me.danlowe.boilerplate.model.providers.network.NetworkInfoProvider
import me.danlowe.boilerplate.model.service.retrofit.SwansonQuoteService
import me.danlowe.boilerplate.ui.TestCoroutineContextProvider
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class QuotesViewModelTest {

    private lateinit var ut: QuotesViewModel
    private val coroutineProvider = TestCoroutineContextProvider()
    private lateinit var networkInfoProvider: NetworkInfoProvider
    private lateinit var quoteService: SwansonQuoteService

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        networkInfoProvider = mock()
        quoteService = mock()
        ut = QuotesViewModel(coroutineProvider, networkInfoProvider, quoteService)
    }

    @After
    fun tearDown() {
        coroutineProvider.testContext.cancelAllActions()
        coroutineProvider.testContext.cancel()
    }

    @Test
    fun `ensure initialization`() {
        val escapeException = object: Exception("escapeException") {}

        whenever(networkInfoProvider.isNetworkAvailable()).thenReturn(true)

        val mockDeferred = mock<Deferred<Array<String>>>()
        whenever(quoteService.getQuote()).thenReturn(mockDeferred).then {
            // We're executing in a loop, so we need to escape.
            throw escapeException
        }

        runBlocking {
            whenever(mockDeferred.await()).thenReturn(arrayOf("test"))
        }

        ut.viewInitialize()

        var exceptions = coroutineProvider.testContext.exceptions
        assertEquals("No exceptions should have occurred yet", 0, exceptions.size)

        coroutineProvider.testContext.advanceTimeBy(ut.quoteDelay)
        exceptions = coroutineProvider.testContext.exceptions


        assertEquals("Only one exception should occur", 1, exceptions.size)
        assertEquals("Exception should match provided exception", escapeException, exceptions[0])

        assertEquals("\"test\"", ut.quote.get())
    }

    @Test
    fun `service not called if no network`() {
        whenever(networkInfoProvider.isNetworkAvailable()).thenReturn(false)

        ut.viewInitialize()
        coroutineProvider.testContext.advanceTimeBy(ut.quoteDelay + (ut.quoteDelay / 2)) // add extra time to avoid a race condition
        coroutineProvider.testContext.cancel()

        verify(networkInfoProvider, times(2)).isNetworkAvailable()
        verify(quoteService, times(0)).getQuote()
    }

    @Test
    fun `empty quotes array received`() {
        whenever(networkInfoProvider.isNetworkAvailable()).thenReturn(true)

        val mockDeferred = mock<Deferred<Array<String>>>()
        whenever(quoteService.getQuote()).thenReturn(mockDeferred)
        runBlocking {
            whenever(mockDeferred.await()).thenReturn(arrayOf())
        }

        ut.viewInitialize()

        coroutineProvider.testContext.advanceTimeBy(ut.quoteDelay / 2)
        coroutineProvider.testContext.cancel()

        verify(networkInfoProvider, times(1)).isNetworkAvailable()
        verify(quoteService, times(1)).getQuote()

        assertTrue("Quote should be empty", ut.quote.get()!!.isEmpty())
    }

    @Test
    fun `empty quote received`() {
        whenever(networkInfoProvider.isNetworkAvailable()).thenReturn(true)

        val mockDeferred = mock<Deferred<Array<String>>>()
        whenever(quoteService.getQuote()).thenReturn(mockDeferred)

        runBlocking {
            whenever(mockDeferred.await()).thenReturn(arrayOf(""))
        }

        ut.viewInitialize()

        coroutineProvider.testContext.advanceTimeBy(ut.quoteDelay / 2)
        coroutineProvider.testContext.cancel()

        verify(networkInfoProvider, times(1)).isNetworkAvailable()
        verify(quoteService, times(1)).getQuote()

        assertTrue("Quote should be empty", ut.quote.get()!!.isEmpty())
    }

    @Test
    fun `ensure continue after TimeoutCancellationException`() {

        whenever(networkInfoProvider.isNetworkAvailable()).then {
            throw TimeoutCancellationException("")
        }

        ut.viewInitialize()

        coroutineProvider.testContext.advanceTimeBy(ut.quoteDelay + ut.quoteDelay / 2)
        coroutineProvider.testContext.cancel()


        verify(networkInfoProvider, times(2)).isNetworkAvailable()
    }
}