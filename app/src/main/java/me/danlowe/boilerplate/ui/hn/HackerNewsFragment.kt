package me.danlowe.boilerplate.ui.hn

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_hacker_news.*
import me.danlowe.boilerplate.R
import me.danlowe.boilerplate.databinding.FragmentHackerNewsBinding
import me.danlowe.boilerplate.model.data.HackerNewsItem
import me.danlowe.boilerplate.ui.base.BaseFragment
import me.danlowe.boilerplate.ui.base.InfiniteScrollListener
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class HackerNewsFragment: BaseFragment<FragmentHackerNewsBinding, HackerNewsViewModel>(), HackerNewsNavigator {
    override val fragViewModel: HackerNewsViewModel by viewModel()
    override val layoutId: Int = R.layout.fragment_hacker_news

    private val newsViewAdapter: HackerNewsViewAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        retainInstance = true
        super.onCreate(savedInstanceState)
        fragViewModel.navigator = this

        lifecycle.addObserver(fragViewModel)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeStories()
        initView()
    }

    private fun observeStories() {
        val storiesObserver = Observer<MutableList<HackerNewsItem>> @Synchronized {

            var curStories = newsViewAdapter.stories.size

            // Ensure current stories isn't larger than size of current list
            if (curStories > it.size) {
                newsViewAdapter.stories.clear()
                curStories = 0
            }
            if (it.size > curStories) {
                newsViewAdapter.stories.addAll(it.subList(curStories, it.size))
                newsViewAdapter.notifyItemInserted(curStories)
            }
        }

        fragViewModel.stories.observe(this, storiesObserver)
    }

    private fun initView() {

        hackerNewsRecyclerView.apply {
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()

            layoutManager = LinearLayoutManager(context)

            val infiniteScrollListener = object: InfiniteScrollListener(layoutManager as LinearLayoutManager) {
                override fun loadMore() {
                    fragViewModel.loadStories(5)
                }
            }

            clearOnScrollListeners()
            addOnScrollListener(infiniteScrollListener)

            adapter = newsViewAdapter
        }
    }

}