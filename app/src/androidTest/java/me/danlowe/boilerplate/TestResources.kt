package me.danlowe.boilerplate

import androidx.test.rule.ActivityTestRule
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.Job
import me.danlowe.boilerplate.ui.main.MainActivity

abstract class TestResources {

    private val testJob = Job()

    /**
     * Creates a scope for the test resource
     */
    protected fun scope(): CoroutineScope {
        return CoroutineScope(Dispatchers.Unconfined + testJob)
    }

    /**
     *  Performs any cleanup tasks associated with a test resource
     */
    open fun cleanup() {
        testJob.cancel()
    }

    open fun setup(testRule: ActivityTestRule<MainActivity>) { }
}