package me.danlowe.boilerplate.model.providers.network

import org.koin.dsl.module.module

val networkModule = module {
    single { MainNetworkInfoProvider(get()) as NetworkInfoProvider }
}