package me.danlowe.boilerplate.ui.hn

import androidx.test.rule.ActivityTestRule
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import me.danlowe.boilerplate.TestResources
import me.danlowe.boilerplate.model.data.HackerNewsItem
import me.danlowe.boilerplate.model.providers.network.NetworkInfoProvider
import me.danlowe.boilerplate.model.service.retrofit.HackerNewsService
import me.danlowe.boilerplate.ui.main.MainActivity
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.loadKoinModules

class HackerNewsTestResources(
        val stories: HashMap<Long, HackerNewsItem> = hashMapOf(),
        val networkInfoResponses: MutableList<Boolean> = mutableListOf()
) : TestResources() {

    var hackerNewsService = object: HackerNewsService {
        override fun getTopStories(): Deferred<Array<Long>> {
            return scope().async {
                stories.keys.toTypedArray()
            }
        }

        override fun getItem(id: Long): Deferred<HackerNewsItem?> {
            return scope().async {
                stories[id]
            }
        }
    }

    var networkInfoProvider = object: NetworkInfoProvider {
        override fun isNetworkAvailable(): Boolean {
            return when {
                networkInfoResponses.size > 1 -> networkInfoResponses.removeAt(0)
                networkInfoResponses.size == 1 -> networkInfoResponses[0]
                else -> false
            }
        }
    }

    val hackerNewsModule = module {
        single(override = true) { hackerNewsService }
        single(override = true) { networkInfoProvider }
    }

    override fun setup(testRule: ActivityTestRule<MainActivity>) {
        testRule.runOnUiThread { loadKoinModules(listOf(hackerNewsModule)) }
        super.setup(testRule)
    }
}