package me.danlowe.boilerplate.model.providers.network

import android.content.Context
import android.net.ConnectivityManager

class MainNetworkInfoProvider(val context: Context): NetworkInfoProvider {

    override fun isNetworkAvailable(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        val isConnected = cm?.activeNetworkInfo?.isConnected
        return isConnected != null && isConnected
    }
}