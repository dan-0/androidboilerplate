package me.danlowe.boilerplate.ui.hn

import androidx.lifecycle.MutableLiveData
import me.danlowe.boilerplate.model.data.HackerNewsItem
import me.danlowe.boilerplate.model.providers.coroutine.CoroutineContextProvider
import me.danlowe.boilerplate.model.providers.network.NetworkInfoProvider
import me.danlowe.boilerplate.model.service.retrofit.HackerNewsService
import me.danlowe.boilerplate.ui.base.BaseViewModel
import timber.log.Timber

class HackerNewsViewModel(
        private val networkInfoProvider: NetworkInfoProvider,
        contextProvider: CoroutineContextProvider,
        private val hackerNewsService: HackerNewsService)
    : BaseViewModel<HackerNewsNavigator>(contextProvider) {

    private val storyIds = mutableListOf<Long>()
    val stories = MutableLiveData<MutableList<HackerNewsItem>>().apply { value = mutableListOf() }

    override fun initialize() {
        super.initialize()
        Timber.d("Initializing")
        launchMain {
            val newIds = fetchNewIds()
            storyIds.clear()
            storyIds.addAll(newIds)

            loadStories(10)
        }
    }

    private suspend fun fetchNewIds(): Array<Long> {

        Timber.d("Loading Stories")
        return if (networkInfoProvider.isNetworkAvailable()) {
            hackerNewsService.getTopStories().await()
        } else {
            Timber.d("Network not connected, retying in 10 seconds")
            arrayOf()
        }
    }

    @Synchronized
    fun loadStories(storiesToLoad: Int) {
        Timber.d("Loading more stories, storyId size: ${storyIds.size}")

        launchMain {
            val newStories = stories.value ?: mutableListOf()
            0.until(storiesToLoad).forEach {
                val newStory = getNewStory()
                newStory ?: return@forEach
                newStories.add(newStory)
            }
            stories.value = newStories
        }
    }

    private suspend fun getNewStory(): HackerNewsItem? {
        if (storyIds.size == 0) {
            Timber.d("No stories left to load")
            return null
        }

        Timber.d("Getting news story: ${storyIds[0]}")
        val id = storyIds.removeAt(0)
        return hackerNewsService.getItem(id).await()
    }
}