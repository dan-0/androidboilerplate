package me.danlowe.boilerplate.ui.hn

import android.content.Intent
import android.net.Uri
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4
import me.danlowe.boilerplate.R
import me.danlowe.boilerplate.R.id.hackerNewsLayout
import me.danlowe.boilerplate.R.id.hackerNewsRecyclerView
import me.danlowe.boilerplate.model.data.HackerNewsItem
import me.danlowe.boilerplate.ui.main.MainActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HackerNewsUiTest {

    @get:Rule
    var activityRule: IntentsTestRule<MainActivity> = IntentsTestRule(MainActivity::class.java)

    private var testResources: HackerNewsTestResources = HackerNewsTestResources()

    init {
        val storyMap: HashMap<Long, HackerNewsItem> = hashMapOf()
        (0 until 5L).associateByTo(storyMap, { it }, {
            when(it) {
                0L -> HackerNewsItem(id = it, title = "Item ${it + 1}", url = "https://google.com")
                else -> HackerNewsItem(id = it, title = "Item ${it + 1}")
            }

        })
        testResources.stories.putAll(storyMap)
        testResources.networkInfoResponses.add(true)
        testResources.setup(activityRule)
    }

    @After
    fun cleanup() {
        testResources.cleanup()
    }

    @Test
    fun testHnViewDisplaysAtStart() {
        onView(withId(hackerNewsLayout)).check(matches(isDisplayed()))
    }

    @Test
    fun testFiveStories() {
        onView(withId(hackerNewsLayout)).check(matches(isDisplayed()))

        onView(withId(hackerNewsRecyclerView))
                .perform(scrollToPosition<HackerNewsViewAdapter.ViewHolder>(4))
                .perform(actionOnItemAtPosition<HackerNewsViewAdapter.ViewHolder>(4, click()))

        onView(withText(R.string.cannot_access))
                .check(matches(isDisplayed()))
    }

    @Test
    fun clickAndOpenBrowser() {
        onView(withId(hackerNewsLayout)).check(matches(isDisplayed()))

        onView(withId(hackerNewsRecyclerView))
                .perform(actionOnItemAtPosition<HackerNewsViewAdapter.ViewHolder>(0, click()))

        intended(allOf(
                hasAction(equalTo(Intent.ACTION_VIEW)),
                hasData(Uri.parse(testResources.stories[0L]!!.url))
        ))
    }
}