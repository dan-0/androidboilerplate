package me.danlowe.boilerplate.ui.images

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val imagesModule = module {
    viewModel { ImagesViewModel(get()) }
    single { ImagesAdapter(mutableListOf()) }
}