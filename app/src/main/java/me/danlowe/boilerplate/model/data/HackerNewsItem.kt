package me.danlowe.boilerplate.model.data

data class HackerNewsItem(
        val by: String = "",
        val descendants: Int = 0,
        val id: Long = 0,
        val kids: IntArray = intArrayOf(),
        val score: Int = 0,
        val time: Long = 0,
        val title: String = "",
        val type: String = "",
        val url: String = ""
)