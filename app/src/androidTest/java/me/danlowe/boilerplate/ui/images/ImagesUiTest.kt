package me.danlowe.boilerplate.ui.images

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import me.danlowe.boilerplate.R.id.imagesLayout
import me.danlowe.boilerplate.R.id.navigation_images
import me.danlowe.boilerplate.ui.main.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ImagesUiTest {

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(
            MainActivity::class.java)

    @Test
    fun testImagesViewDisplays() {
        Espresso.onView(withId(navigation_images)).perform(click())
        Espresso.onView(withId(imagesLayout)).check(matches(isDisplayed()))
    }
}