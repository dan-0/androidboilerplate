package me.danlowe.boilerplate.ui.images

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import me.danlowe.boilerplate.ui.TestCoroutineContextProvider
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ImagesViewModelTest {

    private lateinit var ut: ImagesViewModel
    private val coroutineContextProvider = TestCoroutineContextProvider()

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        ut = ImagesViewModel(coroutineContextProvider)
    }

    @Test
    fun addItems() {
        assertEquals(0, ut.observableItems.value!!.size)

        ut.addItems(999)
        assertEquals(999, ut.observableItems.value!!.size)

        ut.addItems(100)
        assertEquals(1000, ut.observableItems.value!!.size)
    }

    @Test
    fun `test initialization`() {
        ut.viewInitialize()

        assertEquals(5, ut.observableItems.value!!.size)
    }


}