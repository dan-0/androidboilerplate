package me.danlowe.boilerplate.model.providers.coroutine

import kotlin.coroutines.experimental.CoroutineContext

interface CoroutineContextProvider {
//    val io: CoroutineScope
//    val ui: CoroutineScope
    val io: CoroutineContext
    val ui: CoroutineContext
}
